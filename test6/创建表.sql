CREATE TABLE PRODUCT (
	PRODUCT_NAME VARCHAR2 ( 50 ) NOT NULL,
	PRODUCT_ID NUMBER ( 6, 0 ) NOT NULL PRIMARY KEY,
	PRODUCT_PRICE NUMBER ( 6, 0 ) NOT NULL,
	NNUMBER NUMBER ( 6, 0 ) NOT NULL 
);-- EMPLOYEE
CREATE TABLE EMPLOYEE (
	EMPLOYEE_ID NUMBER ( 6, 0 ) PRIMARY KEY,
	ENAME VARCHAR2 ( 50 ) NOT NULL,
	EMAIL VARCHAR2 ( 50 ),
	PHONE_NUMBER NUMBER ( 6, 0 ),
	SEX VARCHAR2 ( 50 ) NOT NULL,
	SALARY NUMBER ( 8, 2 ) NOT NULL 
);
CREATE INDEX EMPLOYEE_NAME_IDX ON EMPLOYEE ( ENAME );
--g ouwuche CREATE TABLE SHOPPINGCAR (
	USERID NUMBER ( 6, 0 ) NOT NULL,
	PRODUCT_NAME VARCHAR2 ( 40 BYTE ) NOT NULL,
	ORDER_DATE DATE NOT NULL,
	PRODUCT_ID NUMBER ( 6, 0 ) NOT NULL,
	BUY_NUMBER NUMBER ( 8, 2 ) DEFAULT 0,
	PRICE NUMBER ( 8, 2 ) DEFAULT 0,
	CONSTRAINT ORDERS_PK PRIMARY KEY ( USERID ) 
	) TABLESPACE USERS PCTFREE 10 INITRANS 1 STORAGE ( BUFFER_POOL DEFAULT ) NOCOMPRESS NOPARALLEL PARTITION BY RANGE ( ORDER_DATE ) (
	PARTITION PARTITION_BEFORE_2016
	VALUES
		LESS THAN (
		TO_DATE ( ' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN' )) NOLOGGING TABLESPACE USERS PCTFREE 10 INITRANS 1 STORAGE ( INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS UNLIMITED BUFFER_POOL DEFAULT ) NOCOMPRESS NO INMEMORY,
		PARTITION PARTITION_BEFORE_2020
	VALUES
		LESS THAN (
		TO_DATE ( ' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN' )) NOLOGGING TABLESPACE USERS,
		PARTITION PARTITION_BEFORE_2021
	VALUES
		LESS THAN (
		TO_DATE ( ' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN' )) NOLOGGING TABLESPACE USERS 
	);
--以后再逐年增加新年份的分区 ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES
	LESS THAN (
	TO_DATE ( '2022-01-01', 'YYYY-MM-DD' )) TABLESPACE USERS;-- DDL for Table ORDERID TEMP
CREATE GLOBAL TEMPORARY TABLE "TEMP" ( "USERID" NUMBER ( 10，0 ) NOT NULL ENABLE, CONSTRAINT "TEMP_PK" PRIMARY KEY ( "USERID" ) ENABLE ) ON COMMIT DELETE ROWS;
COMMENT ON TABLE "TEMP" IS '用 于触发器存储临时USERID';