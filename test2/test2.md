
# 实验2 用户及权限管理 #
## 姓名：成林颖  学号：202010414202 ##

### 步骤1：以system登录到pdborcl，创建角色cly和用户mm，并授权和分配空间 ###


	SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 09:39:31 2023

	Copyright (c) 1982, 2016, Oracle.  All rights reserved.
	
	上次成功登录时间: 星期二 4月  18 2023 09:35:44 +08:00
	
	连接到: 
	Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production
	
	SQL> CREATE ROLE cly;                 
	
	角色已创建。

	
	
	SQL> GRANT connect,resource,CREATE VIEW TO cly;        
	
	授权成功。
	
	SQL> CREATE USER mm IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp; 
	
	用户已创建。
	
	SQL> ALTER USER mm default TABLESPACE "USERS"; 
	
	用户已更改。
	
	SQL> ALTER USER mm QUOTA 50M ON users; 
	
	用户已更改。
	
	SQL> GRANT cly TO mm; 
	
	授权成功。

### 步骤2：新用户mm连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。 ###

	[oracle@oracle1 ~]$ sqlplus mm/123@pdborcl
	
	SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 09:49:11 2023
	
	Copyright (c) 1982, 2016, Oracle.  All rights reserved.
	
	
	连接到: 
	Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production
	
	SQL> show user;
	USER 为 "MM"
	SQL> SELECT * FROM session_privs;
	
	PRIVILEGE
	----------------------------------------
	CREATE SESSION
	CREATE TABLE
	CREATE CLUSTER
	CREATE VIEW
	CREATE SEQUENCE
	CREATE PROCEDURE
	CREATE TRIGGER
	CREATE TYPE
	CREATE OPERATOR
	CREATE INDEXTYPE
	SET CONTAINER
	
	已选择 11 行。
	
	SQL> SELECT * FROM session_roles;
	
	ROLE
	--------------------------------------------------------------------------------
	CLY
	CONNECT
	RESOURCE
	SODA_APP
	
	CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
	INSERT INTO customers(id,name)VALUES(1,'zhang');
	INSERT INTO customers(id,name)VALUES (2,'wang');
	CREATE VIEW customers_view AS SELECT name FROM customers;
	
	表已创建。
	
	GRANT SELECT ON customers_view TO hr;
	SELECT * FROM customers_view;
	
	已创建 1 行。
	
	SQL> 
	已创建 1 行。
	
	SQL> 
	视图已创建。
	
	SQL> 
	授权成功。
	
	SQL> 
	NAME
	--------------------------------------------------
	zhang
	wang


#### 步骤3：用户hr连接到pdborcl，查询mm授予它的视图customers_view ####

    SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 09:52:38 2023

	Copyright (c) 1982, 2016, Oracle.  All rights reserved.
	
	上次成功登录时间: 星期二 4月  18 2023 09:37:25 +08:00
	
	连接到: 
	Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production
	
	SQL> SELECT * FROM mm.customers;
	SELECT * FROM mm.customers
	                 *
	第 1 行出现错误:
	ORA-00942: 表或视图不存在
	
	
	SQL> SELECT * FROM mm.customers_view;
	
	NAME
	--------------------------------------------------
	zhang
	wang

    
## 概要文件设置,用户最多登录时最多只能错误3次 ##
	
	SQL> ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
	
	配置文件已更改

	SQL>  alter user sale  account unlock;
	
	用户已更改。
## 查看数据库的使用情况 ##
#### 查看表空间的数据库文件，以及每个文件的磁盘占用情况。 ####
	SQL> SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';
	
	TABLESPACE_NAME
	------------------------------
	FILE_NAME
	--------------------------------------------------------------------------------
		MB     MAX_MB AUT
	---------- ---------- ---
	USERS
	/home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf
		 5 32767.9844 YES
	
	
	SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
	 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
	 Round(( total - free )/ total,4)* 100 "使用率%"
	 from (SELECT tablespace_name,Sum(bytes)free
	        FROM   dba_free_space group  BY tablespace_name)a,
	       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
	        group  BY tablespace_name)b
	  8   where  a.tablespace_name = b.tablespace_name;
	
	表空间名                           大小MB     剩余MB     使用MB    使用率%
	------------------------------ ---------- ---------- ---------- ----------
	SYSAUX				      440    89.1875   350.8125      79.73
	UNDOTBS1			      100	63.5	   36.5       36.5
	USERS					5     3.9375	 1.0625      21.25
	SYSTEM				      260     7.6875   252.3125      97.04
	USERS02 			       10	   9	      1 	10
	
## 实验总结 ##

*本次实验后对用户以及权限管理有了更深的认识。
1、掌握用户的创建及一般信息的设置。
2、了解Oracle常用的系统权限、角色及用户的概念。
3、掌握界面方式对用户的系统权限和限额进行设置。
4、掌握用grant、revoke对用户属于权限及撤销权限。
5、掌握使用alter user命令进行限额设置。*
